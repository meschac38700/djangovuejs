from .models import Blogger
from rest_framework import routers, serializers
class BloggerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Blogger
        fields= ['id', 'title', 'body']