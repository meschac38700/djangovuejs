from django.contrib import admin
from .models import Blogger
# Register your models here.
class BloggerAdmin(admin.ModelAdmin):
    pass

admin.site.register(Blogger,BloggerAdmin)