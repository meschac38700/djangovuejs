from django.contrib.auth.models import User
from rest_framework import routers, serializers
from django.contrib.auth.hashers import make_password

class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(
        # write_only=True,
        required=True,
        help_text='Leave empty if no change needed',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )
    is_superuser = serializers.BooleanField(
        read_only=True
    )
    class Meta:
        model = User
        fields= ['id', 'first_name', 'last_name', 'username', 'password', 'email', 'is_superuser', 'is_staff']
    
    # To hash password
    # validate_password = make_password
    # OR
    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        return super(UserSerializer, self).create(validated_data)