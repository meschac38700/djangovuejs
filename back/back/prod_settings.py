from .settings import *
import dj_database_url

SECRET_KEY = 'eliamd#)$m%6wr9$()$=89w0)0%%vrr5wekxc4_1z@z7$a9dt$ys&tmmeschac'
DEBUG = False
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = ["eliam-djangovuejs.herokuapp.com"]
CORS_ORIGIN_WHITELIST = ['https://eliam-lotonga.fr', 'https://eliam-django-vuejs.netlify.com',]
# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
DATABASES['default'] = dj_database_url.config()
"""DATABASES['default'] = dj_database_url.config(
    'postgres://phfxwbcbsoxhwi:e50c2d32e7bac619a66a3e3e88531c2bd095707f672257340db975a5679936a2@ec2-46-137-84-173.eu-west-1.compute.amazonaws.com:5432/d9lv5bcdessg6o',
    conn_max_age=600)"""
"""DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'd9lv5bcdessg6o',
        'USER': 'phfxwbcbsoxhwi',
        'PASSWORD': 'e50c2d32e7bac619a66a3e3e88531c2bd095707f672257340db975a5679936a2',
        'HOST': 'ec2-46-137-84-173.eu-west-1.compute.amazonaws.com',
        'PORT': '5432',
    }
}"""

MIDDLEWARE += ['whitenoise.middleware.WhiteNoiseMiddleware']
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# REST_FRAMEWORK = {
#     # Use Django's standard `django.contrib.auth` permissions,
#     # or allow read-only access for unauthenticated users
#     'DEFAULT_PERMISSION_CLASSES':[
#         'rest_framework.permissions.HasAPIKey',
#         #'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
#     ]
# }

# REST_FRAMEWORK
REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework_api_key.permissions.HasAPIKey",
    ]
}

#API_KEY_CUSTOM_HEADER = "HTTP_X_API_KEY"
