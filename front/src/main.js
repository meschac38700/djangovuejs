import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

// Define a root url
Vue.http.options.root = process.env.VUE_APP_BASE_URL;
// Set API KEY
Vue.http.headers.common['Authorization'] = process.env.VUE_APP_API_KEY;

// Set an after request interceptor
Vue.http.interceptors.push( (request, next)=>{
  next((response)=>{
    // define after callback
    if( request.after )
    {
      // this = the current Vue instance
      request.after.call(this, response);
    }
  });
});
Vue.prototype.loading = false;
Vue.prototype.alert = false;

let usersActions = {
  getUsers: {method: 'GET', url:'users/'},
  getUser: {method: 'GET', url:'users{/id}/'},
  postUser: {method: 'POST', url:'users/'},
  updateUser: {method: 'PUT', url:'users{/id}/'},
  deleteUser: {method: 'DELETE', url:'users{/id}/'},

  getBloggers: {method: 'GET', url:'bloggers'},
  postBlogger: {method: 'POST', url:'bloggers/'},
  getBlogger: {method: 'GET', url:'bloggers{id}/'},
  updateBlogger: {method: 'PUT', url:'bloggers{/id}/'},
  deleteBlogger: {method: 'DELETE', url:'bloggers{/id}/'},
}

// Creating a global resource
Vue.prototype.$customResource = Vue.resource('toto', {}, usersActions);

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
